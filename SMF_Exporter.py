import bpy
import types
import struct
from enum import IntEnum

from bpy.props import(
	StringProperty,
	BoolProperty,
	FloatProperty,
	EnumProperty,
	CollectionProperty,
)

from bpy_extras.io_utils import(
	ImportHelper,
	ExportHelper,
	orientation_helper,
	path_reference_mode,
	axis_conversion,
)



bl_info = {
	"name": "Blender SMF Exporter",
	"author": "virattus",
	"version": (0, 5),
	"blender": (3, 6, 0),
	"category": "Import-Export",
	"location": "File > Export",
	"description": "Export scene as Sega Saturn .SMF",
}


MAGIC_VER_2 = 0xDEADBEEF
MAGIC_VER_3 = 0xABBAABBA
MAGIC_VER_4 = 0x12345678
MAGIC_VER_5 = 0x12211535

MAGIC_CUR = MAGIC_VER_5

BINHEADER_SIZE = 28


class ObjectExporter(bpy.types.Operator):
	bl_idname = "export_scene.smf"
	bl_description = "Saturn SMF Exporter"
	bl_label = "Export SMF"
	bl_options = {"REGISTER", "UNDO"}

	filename_ext = ".smf"
	filter_glob: StringProperty(default="*.smf", options={"HIDDEN"})

	filepath: StringProperty(
		subtype="FILE_PATH",
		default="file" + filename_ext,
		)

	use_combine: BoolProperty(
		name="Combine meshes",
		description="Export all objects as one mesh",
		default=False,
		)

	scale: FloatProperty(
		name="Default Scale",
		default=1.0
		)

	SortByUVs: BoolProperty(
		name='Make UVs Accurate',
		description='Sort vertices by UV, requires UVs to be aligned to texture corners',
		default=True
		)

	export_cfile: BoolProperty(
		name='Export C file',
		description="Export to .C file as well",
		default=False
		)

	class ReadDirection(IntEnum):
		READ_DIR_NORMAL = 0
		READ_DIR_H = 1
		READ_DIR_V = 2
		READ_DIR_HV = 3
	
	class SortType(IntEnum):
		SORT_TYPE_BFR = 0
		SORT_TYPE_MIN = 4
		SORT_TYPE_MAX = 8
		SORT_TYPE_CENTRE = 16
	
	class PlaneType(IntEnum):
		PLANE_TYPE_SINGLE = 0
		PLANE_TYPE_DOUBLE = 2
	
	
	
	VERTEX_SIZE = 12
	NORMAL_SIZE = 12
	COLOUR_SIZE = 2
	ATTRIBUTE_SIZE = 12
	POLYGON_SIZE = 10


	meshArray = []
	textures = []
	
	
	def GetReadDirection(self, indices, UVs):		
		rounded = []	
		for i in UVs:
			rounded.append([abs(round(i[0])), abs(round(i[1]))])
					
		if rounded[0][0] == 0.0 and rounded[0][1] == 0.0: #A = Upper Left
			if rounded[1][0] == 0.0 and rounded[1][1] == 1.0: #B = Lower Left
				return self.ReadDirection.READ_DIR_V

		if rounded[0][0] == 0.0 and rounded[0][1] == 1.0: #A = Lower Left
			if rounded[1][0] == 1.0 and rounded[1][1] == 1.0: #B = Lower Right
				return self.ReadDirection.READ_DIR_V
				
		if rounded[0][0] == 1.0 and rounded[0][1] == 0.0: #A = Upper Right
			if rounded[1][0] == 0.0 and rounded[1][1] == 0.0: #B = Upper Left
				return self.ReadDirection.READ_DIR_H

		if rounded[0][0] == 1.0 and rounded[0][1] == 1.0: #A = Lower Right
			if rounded[1][0] == 0.0 and rounded[1][1] == 1.0: #B = Lower Left
				return self.ReadDirection.READ_DIR_H
		
		if rounded[0][0] == 1.0 and rounded[0][1] == 1.0: #A = Lower Right
			if rounded[1][0] == 0.0 and rounded[1][1] == 0.0: #B = Upper Left
				return self.ReadDirection.READ_DIR_HV
		
		return self.ReadDirection.READ_DIR_NORMAL



	#https://blender.stackexchange.com/questions/148388/python-in-2-80-how-to-get-the-base-color-texture-from-a-material	
	def GetTextureID(self, material):
		try:
			nodes = material.node_tree.nodes
			principled = next(n for n in nodes if n.type == "BSDF_PRINCIPLED")
			
			base_color = principled.inputs["Base Color"]
			
			link = base_color.links[0]
			link_node = link.from_node
			
			texture_name = link_node.image.name
			if texture_name in self.textures:
				return self.textures.index(texture_name)
			else:
				self.textures.append(texture_name)
				return self.textures.index(texture_name)
			#print(link_node.image.name)
			
		except:
			print("couldn't find texture")
		
		return -1

	
	def GetAttributes(self, polygon, mesh, material):
		#build attributes
		attribute = types.SimpleNamespace()
		
		if material is None:
			attribute.doublesided = False
			attribute.textureID = 0
			attribute.use_texture = False
			attribute.ReadDir = self.ReadDirection.READ_DIR_NORMAL
		
		else:
			attribute.doublesided = material.use_backface_culling
			attribute.textureID = int(self.GetTextureID(material))
			attribute.use_texture = True if attribute.textureID >= 0 else False
			
			if attribute.use_texture == False:
				attribute.textureID = 0 #reset back to zero, don't know what'll happen if this is max short on saturn
		
			#if self.SortByUVs and len(mesh.uv_layers) > 0:
			if self.SortByUVs:# and uv_layers in mesh:
				#recreate the goddamn indices
				indices = []
				for i in polygon.vertices:
					indices.append(i)
					
				UVs = [None] * 4		
				for vert_idx, loop_idx in zip(polygon.vertices, polygon.loop_indices):
					UVs[indices.index(vert_idx)] = mesh.uv_layers.active.data[loop_idx].uv
				attribute.ReadDir = self.GetReadDirection(indices, UVs)
			else:
				attribute.ReadDir = self.ReadDirection.READ_DIR_NORMAL
	
		return attribute


	def DeconstructObject(self, obj):
		mesh = types.SimpleNamespace()
		mesh.name = obj.name
		mesh.vertices = []
		mesh.UVs = []
		mesh.normals = []
		mesh.textureIDs = []
		mesh.colours = []
		mesh.indices = []
		mesh.attributes = []
		mesh.HasVertexColours = False
		
		objMesh = obj.data

		for i in objMesh.vertices:
			verts = []
			for y in i.co:
				fix16 = int(y * self.scale * 65536.0)
				assert abs(fix16) >= abs(int(y)), f"vertex conversion overflow error: {abs(fix16)} vs {abs(int(y))}, try reducing scale"
				verts.append(fix16)
				
			mesh.vertices.append(verts)
		
		assert len(mesh.vertices) == 8
		

		if(len(objMesh.color_attributes) > 0):
			mesh.HasVertexColours = True
			#we have vertex colours
			colour_attr = objMesh.color_attributes
			colourArray = colour_attr[0].data
			for i in colourArray:
				colours = []
				for y in i.color_srgb:
					colours.append(int(y * 31))
				
				meshcolour = 0x8000 + (colours[0] * 1024) + (colours[1] * 32) + colours[2]
				mesh.colours.append(meshcolour)



		for i in objMesh.polygons:
			indices = []
			normals = []
			
			for y in i.normal:
				normals.append(int(y * 65536.0))

			mesh.normals.append(normals)
			
			material = None
			if len(objMesh.materials) > 0:
				material = objMesh.materials[i.material_index]
			attribute = self.GetAttributes(i, objMesh, material)
			mesh.attributes.append(attribute)
			
			
			assert len(i.vertices) == 4, f"Attempt to export mesh with index count of {len(i.vertices)}, only quads supported"
			for y in i.vertices:
				indices.append(y)
				
			polyflags = 0x0000
			polyflags += self.SortType.SORT_TYPE_CENTRE
			if attribute.doublesided:
				polyflags += self.PlaneType.PLANE_TYPE_DOUBLE
			if attribute.use_texture:
				polyflags += 1
			
			indices.append(polyflags)
			mesh.indices.append(indices)


		self.meshArray.append(mesh)



	def WriteCFile(self):
		open(self.filepath + ".c", 'w').close()
		file = open(self.filepath + ".c", 'w')
		
		lines = [
		"#include <mic3d.h>\n\n",
		"#define COLOR1 RGB1555(1, 16, 0, 0)\n",
		"#define COLOR2 RGB1555(1, 0, 16, 0)\n",
		"#define COLOR3 RGB1555(1, 0, 0, 16)\n\n",
		"#define INDICES(a, b, c, d) .indices.p0 = a, .indices.p1 = b, .indices.p2 = c, .indices.p3 = d\n",
		"#define FLAGS(_sort_type, _plane_type, _use_texture) .flags.sort_type = _sort_type, .flags.plane_type = _plane_type, .flags.use_texture = _use_texture\n\n"
		]
		file.writelines(lines)
		
		
		for i in self.meshArray:
			
			lines = ["static const fix16_vec3_t _points_" + i.name + "[" + str(len(i.vertices)) + "] = {\n"]
			for x in i.vertices:
				lines.append("\t{" + str(x[0]) + ", " + str(x[1]) + ", " + str(x[2]) + "},\n")
			lines.append("};\n\n")
			
			lines.append("static const fix16_vec3_t _normals_" + i.name + "[" + str(len(i.normals)) + "] = {\n")
			for x in i.normals:
				lines.append("\t{" + str(x[0]) + ", " + str(x[1]) + ", " + str(x[2]) + "},\n")
			lines.append("};\n\n")
		
			lines.append("static const polygon_t _polygons_" + i.name + "[" + str(len(i.indices)) + "] = {\n")
			for x in i.indices:
				lines.append("\t{ FLAGS(SORT_TYPE_CENTER, PLANE_TYPE_SINGLE, false), INDICES( " + str(x[0]) + ", " + str(x[1]) + ", " +str(x[2]) + ", " + str(x[3]) + " )},\n")
			lines.append("};\n\n")
			
			lines.append("static const attribute_t _attributes_" + i.name + "[" + str(len(i.attributes)) + "] = {\n")
			for x in range(len(i.attributes)):
				lines.append("\t{ .draw_mode.raw = 0x00C4," +
				" .control.command = COMMAND_TYPE_POLYGON, " +
				".control.link_type = LINK_TYPE_JUMP_ASSIGN, " +
				".palette_data.base_color = COLOR1, " +
				".shading_slot = " + str(x) + " },\n")
			lines.append("};\n\n")
			
			
			lines.append(
			"const mesh_t mesh_" + i.name + " = {\n" + 
			"\t.points = _points_" + i.name + ",\n" + 
			"\t.points_count = sizeof(_points_" + i.name + ") / sizeof(*_points_" + i.name + "),\n" + 
			"\t.normals = _normals_" + i.name + ",\n" + 
			"\t.attributes = _attributes_" + i.name + ",\n" + 
			"\t.polygons = _polygons_" + i.name + ",\n" + 
			"\t.polygons_count = sizeof(_polygons_" + i.name + ") / sizeof(*_polygons_" + i.name + "),\n" + 
			"};\n\n"
			)
			
			
			file.writelines(lines)
			
		
		file.close()



	def WriteMeshes(self):
		open(self.filepath, 'w').close() #erase file if exists
		file = open(self.filepath, 'wb')

		file.write(struct.pack("<I", MAGIC_CUR)) #write magic
		file.write(struct.pack("<i", len(self.meshArray))) #write mesh count

		offset = 8 + (BINHEADER_SIZE * len(self.meshArray)) #8 to account for the two above

		for i in self.meshArray:
			#write mesh headers
			file.write(struct.pack("<i", len(i.vertices))) # write vertex count
			file.write(struct.pack("<i", len(i.indices))) # write polygon count

			file.write(struct.pack("<i", offset)) #write offset to vertices
			offset += len(i.vertices) * self.VERTEX_SIZE
			file.write(struct.pack("<i", offset)) # write offset to face normals
			offset += len(i.normals) * self.NORMAL_SIZE

			if i.HasVertexColours:
				file.write(struct.pack("<i", offset)) #write offset to vertex colours
				offset += len(i.colours) * self.COLOUR_SIZE #make sure we do 16 bit colour before using this
			else:
				file.write(struct.pack("<i", -1))
			
			file.write(struct.pack("<i", offset)) #write offset to attributes
			offset += len(i.attributes) * self.ATTRIBUTE_SIZE

			file.write(struct.pack("<i", offset)) #write offset to indices
			offset += len(i.indices) * self.POLYGON_SIZE #four shorts, plus a 2 byte flag


		#now write the mesh data

		for i in self.meshArray:
			for x in i.vertices:
				file.write(struct.pack("<iii", x[0], x[1], x[2]))

			for x in i.normals:
				file.write(struct.pack("<iii", x[0], x[1], x[2]))

			if i.HasVertexColours:
				for x in i.colours:
					file.write(struct.pack("<H", x))

			
			for x in i.attributes: # 12 bytes always
				file.write(struct.pack("<HHII", x.ReadDir, x.textureID, 64, 128))

			for x in i.indices:
				file.write(struct.pack("<HHHHH", x[4], x[0], x[1], x[2], x[3])) #flags, then indices

		file.close()
		
		#we'll make a new file and save the names of the textures in order
		file = open(self.filepath + ".textures", "w")
		for x in self.textures:
			print(x, file=file)
		file.close()


	def execute(self, context):
		del self.meshArray[:]
		assert len(self.meshArray) == 0
		del self.textures[:]
		
		scene = bpy.context.scene

		for obj in scene.objects:
			self.DeconstructObject(obj)  

		self.WriteMeshes()  
		
		if self.export_cfile:
			self.WriteCFile()

		return {"FINISHED"}

	def invoke(self, context, event):
		context.window_manager.fileselect_add(self)
		return {"RUNNING_MODAL"}


def menu_func(self, context):
	self.layout.operator(ObjectExporter.bl_idname, text="Saturn Model Format (.smf)")


def register():
	bpy.utils.register_class(ObjectExporter)
	bpy.types.TOPBAR_MT_file_export.append(menu_func)


def unregister():
	bpy.utils.unregister_class(ObjectExporter)


if __name__ == "__main__":
	register()
